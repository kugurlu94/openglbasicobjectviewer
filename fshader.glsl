#version 450 

out vec4 fColor;

uniform vec4 Color;

void main() 
{ 
    fColor = Color;
} 
