#ifndef __APP_H
#define __APP_H

#include <string>
#include <Angel.h>
#include "obj.h"

using namespace std;

// Possible colors.
enum color_t {c_black, c_red, c_yellow, c_green, c_blue, c_magenta, c_white, c_cyan};


void appInit(int *, char **);
void appStart();

class ApplicationWindow {
    protected:
        string windowTitle;
        int width, height;
        int windowHandle;
        GLuint program;

        // Menu handles
        int mainMenuHandle;
        int changeObjectMenuHandle;
        int changePolygonModeMenuHandle;
        int changeColorMenuHandle;

        // uniform variable locations
        GLuint modelViewMarixLocation;
        GLuint projectionMatrixLocation;
        GLuint colorLocation;

        // trasformation matrices
        mat4 modelViewMatrix;
        mat4 projectionMatrix;

        // current color
        color_t c_color = c_white;

        // Variable storing the object state.
        GLObjectState o_state;
        GLObject * c_object = nullptr; // current object on display
    public:
        ApplicationWindow(const string& w_title, const int& w_height, const int& w_width);
        void changeObject(const unsigned int index);
        void changeDrawMode(const unsigned int value);
        void changeColor(const color_t color);
        void render();
        void resize(int width, int height);
        void keyControl(unsigned char key, int x, int y);
        void specialKeyControl(int key, int x, int y);

    private:
        void initMenus();
        void refreshView();
};
#endif
