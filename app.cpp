#include <iostream>
#include <Angel.h>
#include "app.h"
#include "obj.h"

using namespace std;

// current ApplicationWindow on the focus.
// used for obtaining callback registrations.
// This works as a singleton, and used for
// adding object-orientism to C OpenGL API.
ApplicationWindow* c_appWindow;

const string WINDOW_TITLE = "Object Preview";
const char* VERTEX_SHADER =  "vshader.glsl";
const char* FRAGMENT_SHADER = "fshader.glsl";

const char* MAN_OFF_FILE = "assets/shapeX.off";
const char* TEAPOT_OFF_FILE = "assets/teapot.off";

// Default window sizes.
const int 
    APP_WINDOW_WIDTH = 600,
    APP_WINDOW_HEIGHT = 600;

// Amount of rotation for each key event.
const GLfloat ROTATION_DELTA = 3.0;

// Amount of zoom for each ket event.
const GLfloat ZOOM_DELTA = 0.1;

// All possible object pointers.
const int NUM_OBJECTS = 3;
const int MAN_INDEX = 0;
const int CUBE_INDEX = 1;
const int TEAPOT_INDEX = 2;
const int DEFAULT_OBJECT_INDEX = MAN_INDEX;
GLObject* ALL_OBJECTS[NUM_OBJECTS];

// Possible draw modes.
const int WIREFRAME_MODE = 0;
const int SOLID_MODE = 1;

const color4 DEFAULT_COLOR = color4(1.0, 1.0, 1.0, 1.0); // default color is white.


void appInit(int* argc, char* argv[]){
    // Initilizes the OpenGL context for the application
    // Global glut options.
    glutInit(argc, argv);
    glutInitContextVersion(4, 5); // I used this version in my system.
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutSetOption(
        GLUT_ACTION_ON_WINDOW_CLOSE,
        GLUT_ACTION_GLUTMAINLOOP_RETURNS
    );
}

/*
    Callback functions to attach for events.
*/

void displayCallback(){
    if(c_appWindow != nullptr)
        c_appWindow->render();
}

void resizeCallback(int width, int height){
    if(c_appWindow != nullptr)
        c_appWindow->resize(width, height);
}

void keyboardCallback(unsigned char key, int x, int y){
    if(c_appWindow != nullptr)
        c_appWindow->keyControl(key, x, y);
}

void specialCallBack(int key, int x, int y){
    if(c_appWindow != nullptr)
        c_appWindow->specialKeyControl(key, x, y);
}

void attachCallbacks(){
    glutDisplayFunc(displayCallback);
    glutReshapeFunc(resizeCallback);
    glutKeyboardFunc(keyboardCallback);
    glutSpecialFunc(specialCallBack);
}

/*
    Callbacks for glut pop-up menus.
*/

void changeObjectCallback(int value){
    if(c_appWindow != nullptr && value < NUM_OBJECTS){
        c_appWindow->changeObject(value);
    }
}

void changePolygonModeCallback(int value){
    if(c_appWindow != nullptr)
        c_appWindow->changeDrawMode(value);
}

void changeColorCallback(int value){
    if(c_appWindow != nullptr){
        c_appWindow->changeColor(color_t(value));
    }
}


void appStart(){
    ApplicationWindow appWindow (WINDOW_TITLE, APP_WINDOW_WIDTH, APP_WINDOW_HEIGHT);
    c_appWindow = &appWindow;
    attachCallbacks();
    // Starts the main event loop.
    glutMainLoop();
    cout << "Bye..." << endl;
}


ApplicationWindow::ApplicationWindow(const string& w_title, const int& w_height, const int& w_width){
    /* Initilizes the application window */
    windowTitle = w_title;
    height = w_height;
    width = w_width;

    glutInitWindowSize(width, height);
    windowHandle = glutCreateWindow(windowTitle.c_str());
    if(windowHandle < 1) {
        cerr << "Cannot create window." << endl;
        exit(EXIT_FAILURE);
    }

    glewExperimental = GL_TRUE;
    GLenum GlewInitResult = glewInit();

    if (GLEW_OK != GlewInitResult) {
        cerr << "ERROR: " << glewGetErrorString(GlewInitResult) << endl;
        exit(EXIT_FAILURE);
    }
    glGetError(); // glew creates a error for some reason.

    // Compiling shaders and generating program
    program = InitShader(VERTEX_SHADER, FRAGMENT_SHADER);
    glUseProgram(program);

    // Get uniform variable locations
    modelViewMarixLocation = glGetUniformLocation(program, "ModelViewMatrix");
    projectionMatrixLocation = glGetUniformLocation(program, "ProjectionMatrix" );
    colorLocation = glGetUniformLocation(program, "Color");

    // Generate Uniform variables.
    modelViewMatrix = o_state.getModelViewMatrix();
    projectionMatrix = Ortho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);
    
    // Set Uniform variables
    glUniformMatrix4fv(modelViewMarixLocation, 1, GL_TRUE, modelViewMatrix);
    glUniformMatrix4fv(projectionMatrixLocation, 1, GL_TRUE, projectionMatrix);
    glUniform4fv(colorLocation, 1, DEFAULT_COLOR);

    // OpenGL configurations.
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glFrontFace(GL_CCW);

    glUseProgram(0);

    this->changeColor(c_color);
    this->changeObject(DEFAULT_OBJECT_INDEX);
    this->initMenus();
}

void ApplicationWindow::initMenus(){
    // Initilizes all the submenus and adds them to main menu.
    glutSetWindow(windowHandle);
    changeObjectMenuHandle = glutCreateMenu(changeObjectCallback);
    glutSetMenu(changeObjectMenuHandle);
    glutAddMenuEntry("Man", MAN_INDEX);
    glutAddMenuEntry("Cube", CUBE_INDEX);
    glutAddMenuEntry("Teapot", TEAPOT_INDEX);

    changePolygonModeMenuHandle = glutCreateMenu(changePolygonModeCallback);
    glutSetMenu(changePolygonModeMenuHandle);
    glutAddMenuEntry("Wireframe", WIREFRAME_MODE);
    glutAddMenuEntry("Solid", SOLID_MODE);

    changeColorMenuHandle = glutCreateMenu(changeColorCallback);
    glutSetMenu(changeColorMenuHandle);

    glutAddMenuEntry("Black", c_black);
    glutAddMenuEntry("Red", c_red);
    glutAddMenuEntry("Yellow", c_yellow);
    glutAddMenuEntry("Green", c_green);
    glutAddMenuEntry("Blue", c_blue);
    glutAddMenuEntry("Magenta", c_magenta);
    glutAddMenuEntry("White", c_white);
    glutAddMenuEntry("Cyan", c_cyan);


    mainMenuHandle = glutCreateMenu(NULL);
    glutSetMenu(mainMenuHandle);
    glutAddSubMenu("Object Type", changeObjectMenuHandle);
    glutAddSubMenu("Drawing Mode", changePolygonModeMenuHandle);
    glutAddSubMenu("Color", changeColorMenuHandle);

    glutAttachMenu(GLUT_RIGHT_BUTTON);
}


// Must be called on each view change(object state change.)
void ApplicationWindow::refreshView(){
    // Reload modelview matrice.
    glUseProgram(program);
    modelViewMatrix = o_state.getModelViewMatrix();
    glUniformMatrix4fv(modelViewMarixLocation, 1, GL_TRUE, modelViewMatrix);
    glUseProgram(0);

    glutPostRedisplay();
}

// Changes the object on the focus.
// Allocates memory lazy.
void ApplicationWindow::changeObject(const unsigned int index){
    GLObject * s_object = ALL_OBJECTS[index];

    // Initilize and allocate memory for uninitialized object.
    if(s_object == nullptr){
        switch(index){
            case MAN_INDEX    : s_object = new GLOffObject(program, MAN_OFF_FILE); break;
            case CUBE_INDEX   : s_object = new GLCube(program); break;
            case TEAPOT_INDEX : s_object = new GLOffObject(program, TEAPOT_OFF_FILE); break;
            default           : return;
        }
        s_object->allocate();
        ALL_OBJECTS[index] = s_object;
    }

    // Already initilized objects are not reallocated.
    this->c_object = s_object;
    glutPostRedisplay();
}

void ApplicationWindow::changeDrawMode(const unsigned int value){
    // Changes the PolygonMode

    glUseProgram(program);
    if (value == WIREFRAME_MODE)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else if (value == SOLID_MODE)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glUseProgram(0);
    glutPostRedisplay();
}

// Changes the current color.
void ApplicationWindow::changeColor(const color_t color){
    c_color = color;
    color4 color_vec;

    switch(c_color)
    {
        case c_black   : color_vec = color4( 0.0, 0.0, 0.0, 1.0 ); break;
        case c_red     : color_vec = color4( 1.0, 0.0, 0.0, 1.0 ); break;
        case c_yellow  : color_vec = color4( 1.0, 1.0, 0.0, 1.0 ); break;
        case c_green   : color_vec = color4( 0.0, 1.0, 0.0, 1.0 ); break;
        case c_blue    : color_vec = color4( 0.0, 0.0, 1.0, 1.0 ); break;
        case c_magenta : color_vec = color4( 1.0, 0.0, 1.0, 1.0 ); break;
        case c_white   : color_vec = color4( 1.0, 1.0, 1.0, 1.0 ); break;
        case c_cyan    : color_vec = color4( 0.0, 1.0, 1.0, 1.0 ); break;
        default        : color_vec = DEFAULT_COLOR; break;
    }

    glUseProgram(program);
    glUniform4fv(colorLocation, 1, color_vec);
    glUseProgram(0);

    glutPostRedisplay();
}

// Renders the current object on the focus.
void ApplicationWindow::render(){
    // The rendering function.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Any drawing should be done here
    if (c_object != nullptr){
        c_object->draw();
    }

    glutSwapBuffers();
}

// Resizes viewport on window size changes.
void ApplicationWindow::resize(int width, int height){
    this->width = width;
    this->height = height;
    glViewport(0, 0, width, height);
}

// Arrow controls.
void ApplicationWindow::specialKeyControl(int key, int x, int y){
    switch (key){
        case GLUT_KEY_LEFT:
        {
            this->o_state.rotateZ(ROTATION_DELTA);
            this->refreshView();
            break;
        }
        case GLUT_KEY_RIGHT:
        {
            this->o_state.rotateZ(-1.0 * ROTATION_DELTA);
            this->refreshView();
            break;
        }
        case GLUT_KEY_DOWN:
        {
            this->o_state.rotateX(ROTATION_DELTA);
            this->refreshView();
            break;
        }
        case GLUT_KEY_UP:
        {
            this->o_state.rotateX(-1.0 * ROTATION_DELTA);
            this->refreshView();
            break;
        }
        default:
        {
            break;
        }
    }
}

// Keyboard controls.
void ApplicationWindow::keyControl(unsigned char key, int x, int y){
    switch (key)
    {
        case 'z':
            {
                this->o_state.zoomIn(ZOOM_DELTA);
                this->refreshView();
                break;
            }
        case 'Z':
            {
                this->o_state.zoomOut(ZOOM_DELTA);
                this->refreshView();
                break;
            }
        case 'i':
            {
                this->o_state.resetRotation();
                this->refreshView();
                break;
            }
        case 'h':
            {
                cout << endl << "### HELP ###" << endl
                    << "Arrow key  (see below) -- set the object rotation around X-axis (up and down arrow) and around Z-axis (left and right arrow)" << endl
                    << "i -- initialize rotation" << endl
                    << "z -- zoom-in, and Z -- zoom-out" << endl
                    << "Right-Click to see for menu options" << endl
                    << "h -- help, print explanation of your input commands (simply to command line)" << endl
                    << "q -- quit (exit) the program" << endl;
                break;
            }
        case 'q':
            {
                glutLeaveMainLoop();
                break;
            }
        default:
            {
                break;
            }
    }
}