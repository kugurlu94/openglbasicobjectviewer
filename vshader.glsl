#version 450

in  vec4 vPosition;
in  vec4 vColor;

uniform mat4 ModelViewMatrix;
uniform mat4 ProjectionMatrix;

void main() 
{
    gl_Position = ProjectionMatrix * ModelViewMatrix * vPosition;
} 
