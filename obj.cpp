#include <iostream>
#include <cstring>
#include <cstdlib>
#include <vector>
#include <Angel.h>
#include "obj.h"

using namespace std;

/*
    GLObjectState class implementation.
    Class is used for storing the rotation and scale information
    of an object. And used for generating the ModelView matrix for
    object in its current state.   
*/

GLfloat GLObjectState::DEFAULT_X = 0.0;
GLfloat GLObjectState::DEFAULT_Z = 0.0;
GLfloat GLObjectState::DEFAULT_SCALE = 1.0;
GLfloat GLObjectState::MIN_SCALE = 0.2;
GLfloat GLObjectState::MAX_SCALE = 2.0;


// Default constructor.
GLObjectState::GLObjectState(){
    this->x_rotation = DEFAULT_X;
    this->z_rotation = DEFAULT_Z;
    this->scale = DEFAULT_SCALE;
}


// Custom constructor.
GLObjectState::GLObjectState(const GLfloat x_rot, const GLfloat z_rot, const GLfloat scale){
    this->x_rotation = x_rot;
    this->z_rotation = z_rot;
    this->scale = scale;
}

// Rotate and zoom methods implemented very straightforward.
void GLObjectState::rotateX(const GLfloat rot){
    this->x_rotation += rot;
    if (this->x_rotation >= 360.0)
        this->x_rotation -= 360.0;

    if (this->x_rotation < 0.0){
        this->x_rotation += 360.0;
    }
}

void GLObjectState::rotateZ(const GLfloat rot){
    this->z_rotation += rot;
    if (this->z_rotation >= 360.0)
        this->z_rotation -= 360.0;

    if (this->z_rotation < 0.0){
        this->z_rotation += 360.0;
    }
}

void GLObjectState::zoomIn(const GLfloat val){
    GLfloat s_val = scale + val;
    if(s_val <= MAX_SCALE)
        this->scale = s_val;
}

void GLObjectState::zoomOut(const GLfloat val){
    GLfloat s_val = scale - val;
    if(s_val >= MIN_SCALE)
        this->scale = s_val;
}

// reset method for reseting the object to its defualt position.
void GLObjectState::reset(){
    this->resetRotation();
    this->scale = DEFAULT_SCALE;
}

// resetRotation method resets only rotation, leaving scale constant.
void GLObjectState::resetRotation(){
    this->x_rotation = DEFAULT_X;
    this->z_rotation = DEFAULT_Z;
}


mat4 GLObjectState::getModelViewMatrix(){
    return Scale(this->scale, this->scale, this->scale) * RotateX(this->x_rotation) * RotateZ(this->z_rotation);
}


/*
    abstract GLObject class and its subclasses.
*/

const char* GLObject::vPOSITION_ATTRIB = "vPosition";

GLObject::GLObject(GLuint program){
    this->program = program;
    this->vao_id = 0;
    this->vbo_ids[0] = 0;
    this->vbo_ids[1] = 0;
}


/*
    A simple unit cube implementation, placed in the origin.
    Vertices and indices are defualt member variables defined in header file.
    Just implemeted in order to debug easier.
*/

GLCube::GLCube(GLuint program): GLObject(program){}

void GLCube::allocate(){
    glUseProgram(program);
    // Create an initilize vertex array object
    if(vao_id == 0)
        glGenVertexArrays(1, &vao_id);

    glBindVertexArray(vao_id);
    // Create and initialize a buffer object;
    // First index for vertex objects, second for index objects.
    if(vbo_ids[0] == 0 && vbo_ids[1] == 0)
        glGenBuffers(2, &vbo_ids[0]);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_ids[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // set up vertex arrays;
    GLuint vPosition = glGetAttribLocation(program, vPOSITION_ATTRIB);
    glEnableVertexAttribArray(vPosition);
    glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

    // set up index buffer objects.
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_ids[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glBindVertexArray(0);
    glUseProgram(0);
}


void GLCube::draw(){
    if(vao_id != 0){
        glUseProgram(program);
        glBindVertexArray(vao_id);

        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)0);

        glBindVertexArray(0);
        glUseProgram(0);
    }
}


/*
    Implementation of GLOffObject.
*/

// Utility string tokenizer method for tokenizing the offfile data.
vector<string>& tokenize(vector<string>& result, string& str, string& delimiters){
    // Retuns a vector of strings, tokenized from str using chars in the delimiters.
    result.clear();
    size_t current;
    size_t next = -1;
    do
    {
        current = str.find_first_not_of(delimiters, next + 1);
        if (current == string::npos)
            break;
        next = str.find_first_of(delimiters, current);
        result.push_back(str.substr(current, next - current));
    } while (next != string::npos);

    return result;
}


GLOffObject::GLOffObject(GLuint program, string filename) : GLObject(program) {
    // initials in the case parsing file fails.
    this->numVertices = 0;
    this->vertices = NULL;

    this->numIndices = 0;
    this->indices = NULL;

    FILE* fp = fopen(filename.c_str(), "r");

    if ( fp != NULL )
    {   
        cout << "Reading from OFF file " << filename << endl;
        // Accessed the file, read and parse.
        fseek(fp, 0L, SEEK_END);
        long size = ftell(fp);

        fseek(fp, 0L, SEEK_SET);
        char* buf = new char[size + 1];
        fread(buf, 1, size, fp);

        buf[size] = '\0';
        fclose(fp);
        
        // Tokenize the buffered file.
        string str(buf);
        string delimiters(" \n\t\r");
        vector<string> tokenized;
        tokenized = tokenize(tokenized, str, delimiters);

        size_t num_tokens = tokenized.size();

        // Parse tokenized string.
        if (tokenized.size() > 3 && tokenized[0] == "OFF"){
            int num_vertices = stoi(tokenized[1]);

            vector<point4> vertex_vector;

            int i;
            for(i=0; i < num_vertices; i++){
                GLfloat x_val = stof(tokenized[3*i+4]);
                GLfloat y_val = stof(tokenized[3*i+5]);
                GLfloat z_val = stof(tokenized[3*i+6]);
                vertex_vector.push_back(point4(x_val, y_val, z_val, 1));
            }

            vector<GLuint> indices_vector;

            unsigned int offset = 4 + 3*num_vertices;

            while(offset < num_tokens){
                int num_vert_on_face = stoi(tokenized[offset]);
                int num_triangles = num_vert_on_face - 2;
                int face_indices[num_vert_on_face];
                
                for(i = 0; i < num_vert_on_face; i++)
                    face_indices[i] = stoi(tokenized[offset + i + 1]);
    
                // generate a triagles the face.
                for(i = 0; i < num_triangles; i++){
                    indices_vector.push_back(face_indices[0]);
                    indices_vector.push_back(face_indices[i + 1]);
                    indices_vector.push_back(face_indices[i + 2]);
                }

                offset += num_vert_on_face + 1;
            }

            this->numVertices = num_vertices;
            this->numIndices = indices_vector.size();
            this->vertices = vertex_vector.data();
            this->indices = indices_vector.data();
        }
        else
            cout << "Invalid file header." << endl;
    }
    else
        cout << "Can't read OFF file." << endl;
}

void GLOffObject::allocate(){
    if (numVertices > 0 && numIndices > 0){
        glUseProgram(program);

        // Create an initilize vertex array object
        if(vao_id == 0)
            glGenVertexArrays(1, &vao_id);
        glBindVertexArray(vao_id);

        // Create and initialize a buffer object;
        // First index for vertex objects, second for index objects.
        if((vbo_ids[0] == 0) && (vbo_ids[1] == 0))
            glGenBuffers(2, &vbo_ids[0]);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_ids[0]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * numVertices, vertices, GL_STATIC_DRAW);

        // set up vertex arrays;
        GLuint vPosition = glGetAttribLocation(program, vPOSITION_ATTRIB);
        glEnableVertexAttribArray(vPosition);
        glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

        // set up index buffer objects.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_ids[1]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * numIndices, indices, GL_STATIC_DRAW);

        glBindVertexArray(0);
        glUseProgram(0);
    }
}

void GLOffObject::draw(){
    if(vao_id != 0){
        glUseProgram(program);
        glBindVertexArray(vao_id);

        glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, (GLvoid*)0);

        glBindVertexArray(0);
        glUseProgram(0);
    }
}