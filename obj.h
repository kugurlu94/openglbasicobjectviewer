#ifndef __OBJ_H
#define __OBJ_H

#include <iostream>
#include <Angel.h>


typedef Angel::vec4  color4;
typedef Angel::vec4  point4;


// Class that describes a objects state.
// Genereates the model_view matrix.
class GLObjectState {
    protected:
        GLfloat x_rotation;
        GLfloat z_rotation;
        GLfloat scale;
    public:
        static GLfloat DEFAULT_X;
        static GLfloat DEFAULT_Z;
        static GLfloat DEFAULT_SCALE;
        static GLfloat MIN_SCALE;
        static GLfloat MAX_SCALE;

        GLObjectState(); // Default constructor.
        GLObjectState(const GLfloat x_rot, const GLfloat z_rot, const GLfloat scale); // Explicit constructor.
        void rotateX(const GLfloat rot);
        void rotateZ(const GLfloat rot);
        void zoomIn(const GLfloat val);
        void zoomOut(const GLfloat val);
        void resetRotation();
        void reset();
        mat4 getModelViewMatrix();
};


// abstract GLObject used for abstracting the details of
// each type of object to draw
class GLObject {
    protected:
        static const char* vPOSITION_ATTRIB; // Vertex poistion attribute name
        GLuint program; // program object belongs to.
        GLuint vao_id; // vertex array object for storing vertex data.
        GLuint vbo_ids[2]; // vertex buffer objects. index 0 for vertex data, index 1 for index data.
    public:
        GLObject(GLuint program);
        virtual void allocate() =0; // method for allocating memory in gpu
        virtual void draw() =0; // method for drawing the pre-allocated object
};

// Simple cube class for ease of testing.
class GLCube : public GLObject {
    protected:
        // Vertices
        point4 vertices[8] = {
            point4(-.5f, -.5f,  .5f, 1),
            point4(-.5f,  .5f,  .5f, 1),
            point4( .5f,  .5f,  .5f, 1),
            point4( .5f, -.5f,  .5f, 1),
            point4(-.5f, -.5f, -.5f, 1),
            point4(-.5f,  .5f, -.5f, 1),
            point4( .5f,  .5f, -.5f, 1),
            point4( .5f, -.5f, -.5f, 1),
        };

        GLuint indices[36] = {
            0,2,1,  0,3,2,
            4,3,0,  4,7,3,
            4,1,5,  4,0,1,
            3,6,2,  3,7,6,
            1,6,5,  1,2,6,
            7,5,6,  7,4,5
        };

    public:
        GLCube(GLuint program);
        void allocate();
        void draw();
};


// Object which reads the vertex data from a OFF file.
class GLOffObject : public GLObject {
    protected:
        int numVertices; // number of vertices.
        point4* vertices;
        int numIndices; // number of indices.
        GLuint* indices;

    public:
        GLOffObject(GLuint program, std::string filename);
        void allocate();
        void draw();
};
#endif