#include "app.h"

/*
	Note: The project is made to be used on linux platforms.
	There might be incompatiblity issues that I have not addressed.
	I used Ubuntu 14.04, X.Org 1.17.2 and OpenGL 4.5.

	I could not use the glut's teapot and torus functions because
	I could not made it use my shaders. Instead I found an off file
	for a teapot and used it.
*/


/* 
    The entry point of the application.
*/
int main(int argc, char* argv[]){
    appInit(&argc, argv);
    appStart();
}

#ifdef __LINUX__

// Lines below are workaround in order to force
// compiler to link pthread.so, because it won't
// link it for some reason when <string> and <GL/freeglut.h>
// is used at the same time.
#include <pthread.h>

void* someFunc(void*) {}
void forcePThreadLink() {
    pthread_t t1;
    pthread_create(&t1, NULL, &someFunc, NULL);
}
#endif