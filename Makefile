CC = g++
CPPVERSION = -std=c++11
DEBUG = -O2 -g
LDLIBS = -lglut -lGLEW -lGL -lGLU -lpthread

CXXINCS = -I include
COMMON_DIR = common

INIT_SHADER = $(COMMON_DIR)/InitShader.cpp
INIT_SHADER_OBJ = $(COMMON_DIR)/InitShader.o

APP = app.cpp
APP_OBJ = app.o

OBJ = obj.cpp
OBJ_OBJ = obj.o

TARGETS = main

all: $(TARGETS)


.PHONY: clean cleanall

$(TARGETS): $(APP_OBJ) $(OBJ_OBJ) $(INIT_SHADER_OBJ)
	$(CC) $(CPPVERSION) $(DEBUG) $(CXXINCS) $(INIT_SHADER_OBJ) $(APP_OBJ) $(OBJ_OBJ) $@.cpp $(LDLIBS) -o $@

$(APP_OBJ): $(APP)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(APP) -o $(APP_OBJ)

$(OBJ_OBJ): $(OBJ)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(OBJ) -o $(OBJ_OBJ)

$(INIT_SHADER_OBJ): $(INIT_SHADER)
	$(CC) $(CPPVERSION) -c -Wall $(INIT_SHADER) -o $(INIT_SHADER_OBJ)

clean:
	\rm $(TARGETS)

cleanall:
	\rm $(INIT_SHADER_OBJ) $(OBJ_OBJ) $(APP_OBJ) $(TARGETS)
